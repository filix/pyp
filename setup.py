import json
from pathlib import Path

from setuptools import find_packages
from setuptools import setup

THIS_DIR = Path(__file__).parent.resolve()

with (THIS_DIR / 'filip-project.json').open() as f:
    project = json.load(f)

name = project['name']
version = project['version']
python_name = project.get('python_name') or name.replace('-', '_')
python_root = project.get('python_root') or 'src'
python_console_scripts = project.get('python_console_scripts') or []

setup(
    name=python_name,
    version=version,
    package_dir={"": python_root},
    packages=find_packages(where=python_root),
    install_requires=[],
    tests_require=[],
    entry_points={
        "console_scripts": python_console_scripts,
    },
)
