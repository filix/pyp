upstream sources:

- https://code.google.com/archive/p/pyp/ (version 2.11)
- https://github.com/yuvadm/pyp/ (version 2.12)
- https://github.com/djhshih/pyp (version 2.13)
